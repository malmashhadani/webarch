class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	has_secure_password
	#attr_accessible :email, :password_digest, :password, :password_confirmation	
	validate :email, :presence => true, :uniqueness => {:case_sensitive => false}
end
